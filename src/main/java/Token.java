import java.math.BigDecimal;

public abstract class Token {
    private TokenType tokenType;
    private Object value;

    public Token(TokenType tokenType, Object value) {
        this.tokenType = tokenType;
        this.value = value;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public Object getValue() {
        return value;
    }


    public void processStack(CompileContext compileContext) {
        if (!compileContext.getStack().empty()) {
                if (compileContext.getStack().peek().getTokenType() == TokenType.PLUS) {
                    compileContext.getStack().pop();
                    Token pop = compileContext.getStack().pop();

                    setValue(((BigDecimal) pop.getValue()).add((BigDecimal) getValue()));
                    removeLeftParenthesis(compileContext);
                    processStack(compileContext);
                } else if (compileContext.getStack().peek().getTokenType() == TokenType.MINUS) {
                    compileContext.getStack().pop();
                    Token pop = compileContext.getStack().pop();

                    setValue(((BigDecimal) pop.getValue()).subtract((BigDecimal) getValue()));
                    removeLeftParenthesis(compileContext);
                    processStack(compileContext);
                } else if (compileContext.getStack().peek().getTokenType() == TokenType.MUL) {
                    compileContext.getStack().pop();
                    Token pop = compileContext.getStack().pop();

                    setValue(((BigDecimal) pop.getValue()).multiply((BigDecimal) getValue()));
                    removeLeftParenthesis(compileContext);
                    processStack(compileContext);
                } else if (compileContext.getStack().peek().getTokenType() == TokenType.DIV) {
                    compileContext.getStack().pop();
                    Token pop = compileContext.getStack().pop();

                    setValue(((BigDecimal) pop.getValue()).divide((BigDecimal) getValue()));
                    removeLeftParenthesis(compileContext);
                    processStack(compileContext);
                } else {
                    compileContext.getStack().push(this);
                }
        } else {
            compileContext.getStack().push(this);
        }
    }

    private void removeLeftParenthesis(CompileContext compileContext) {
        if (!compileContext.getStack().empty() && compileContext.getStack().peek().getTokenType() == TokenType.LEFT_PARENTHESIS) {
            compileContext.getStack().pop();
        }
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public abstract void interpret(CompileContext compileContext);
}

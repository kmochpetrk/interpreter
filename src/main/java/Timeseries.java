import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Timeseries {
    //@JsonProperty("_time")
    private String time = null;


    private Map<String, Object> fields = new HashMap<>() ;

    public Timeseries time(String time) {
        this.time = time;
        return this;
    }

    /**
     * Get time
     * @return time
     **/


    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public Timeseries fields(Map<String, Object> fields) {
        this.fields = fields;
        return this;
    }

    public Timeseries putFieldsItem(String key, Object fieldsItem) {
        if (this.fields == null) {
            this.fields = new HashMap<String, Object>();
        }
        this.fields.put(key, fieldsItem);
        return this;
    }

    /**
     * Get fields
     * @return fields
     **/


    //@JsonAnyGetter
    public Map<String, Object> getFields() {
        return fields;
    }
    //@JsonAnySetter
    public void setFields(String name, Object value) {
        this.fields.put(name, value);
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Timeseries timeseries = (Timeseries) o;
        return Objects.equals(this.time, timeseries.time) &&
                Objects.equals(this.fields, timeseries.fields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, fields);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Timeseries {\n");

        sb.append("    time: ").append(toIndentedString(time)).append("\n");
        sb.append("    fields: ").append(toIndentedString(fields)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}


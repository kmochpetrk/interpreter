import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Compiler {

    /**
     * process 1 row
     *
     * @param line line to parse
     * @param varsMap variables map to use as values
     * @param timeseries - mdsp timeseries data - only sumby token
     */
    public void compile(String line, Map<String, BigDecimal> varsMap, List<Timeseries> timeseries) {
        final List<Token> tokenList = new ArrayList<>();
        Parser parser = new Parser();
        parser.parseLine(line, tokenList, varsMap);
        System.out.println("Size " + tokenList.size());
        CompileContext compileContext = new CompileContext(tokenList, varsMap);
        compileContext.setTimeseries(timeseries);
        while (compileContext.getCurrentIndex() < tokenList.size()) {

            tokenList.get(compileContext.getCurrentIndex()).interpret(compileContext);
        }
        System.out.println("Result count: " + compileContext.getStack().size());
        System.out.println("Result sum: " + compileContext.getStack().pop().getValue());
    }

}

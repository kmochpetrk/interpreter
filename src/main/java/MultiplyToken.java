public class MultiplyToken extends Token {

    public MultiplyToken() {
        super(TokenType.MUL, "*");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(this);
    }
}

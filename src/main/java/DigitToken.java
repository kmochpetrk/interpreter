import java.math.BigDecimal;

public class DigitToken extends Token {

    public DigitToken(BigDecimal value) {
        super(TokenType.DIGIT, value);
    }

    @Override
    public void interpret(CompileContext compileContext) {
        processStack(compileContext);



        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
    }
}

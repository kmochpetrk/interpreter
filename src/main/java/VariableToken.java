import java.math.BigDecimal;
import java.util.Map;

public class VariableToken extends Token{

    private Map<String, BigDecimal> varsMap;

    public VariableToken(String value, Map<String, BigDecimal> varsMap) {
        super(TokenType.VARIABLE, value);
        this.varsMap = varsMap;
    }

    @Override
    public void interpret(CompileContext compileContext) {
        processStack(compileContext);



        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
    }


    @Override
    public Object getValue() {
        try {
            String value = (String) super.getValue();
            return varsMap.get(value);
        } catch (ClassCastException ex) {
            return super.getValue();
        }

    }

    public String getName() {
        String name = (String) super.getValue();
        return name;
    }
}

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {

    private static Map<String, BigDecimal> varsMap = initMapOfVars();

    private static List<Timeseries> timeseries = new ArrayList<>();

    private static Map<String, BigDecimal> initMapOfVars() {


        Map<String, BigDecimal> returnMap = new HashMap<>();

        returnMap.put("var1", BigDecimal.valueOf(108));
        returnMap.put("var2", BigDecimal.valueOf(111));
        returnMap.put("var3", BigDecimal.valueOf(9));

        return returnMap;

    }


    public static void main(String[] args) {

        Timeseries e1 = new Timeseries();
        e1.setTime("1");
        e1.setFields("order", 1);
        e1.setFields("var2", 100);
        e1.setFields("var3", 1);
        timeseries.add(e1);
        Timeseries e2 = new Timeseries();
        e2.setTime("2");
        e2.setFields("order", 1);
        e2.setFields("var2", 100);
        e2.setFields("var3", 1);
        timeseries.add(e2);
        Timeseries e3 = new Timeseries();
        e3.setTime("3");
        e3.setFields("order", 2);
        e3.setFields("var2", 100);
        e3.setFields("var3", 1);
        timeseries.add(e3);

        final File file = new File(
                Application.class.getClassLoader().getResource("input.txt").getFile());

        try {
            printFile(file);
        } catch (IOException e) {


        }
    }

    private static void printFile(File file) throws IOException {

        if (file == null) return;

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line = br.readLine();
            Compiler compiler = new Compiler();
            compiler.compile(line, varsMap, timeseries);
        }
    }
}

public class MinusToken extends Token {

    public MinusToken() {
        super(TokenType.MINUS, "-");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(this);
    }
}

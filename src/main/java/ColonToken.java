public class ColonToken extends Token{

    public ColonToken() {
        super(TokenType.COLON, ",");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
    }
}

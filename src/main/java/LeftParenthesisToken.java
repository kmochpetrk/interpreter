public class LeftParenthesisToken extends Token{

    public LeftParenthesisToken() {
        super(TokenType.LEFT_PARENTHESIS, "(");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(this);
    }
}

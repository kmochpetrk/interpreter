public class PlusToken extends Token {

    public PlusToken() {
        super(TokenType.PLUS, "+");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(this);
    }
}

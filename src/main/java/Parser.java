import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Parser {

    public void parseLine(String line, List<Token> tokenList, Map<String, BigDecimal> varsMap) {
        String tokenPart = "";
        for (int i = 0; i < line.length(); i++) {
            if (Character.isWhitespace(Character.valueOf(line.charAt(i)))) {

            } else if (Character.valueOf(line.charAt(i)).equals(',')
                    || Character.valueOf(line.charAt(i)).equals('+')
                    || Character.valueOf(line.charAt(i)).equals('-')
                    || Character.valueOf(line.charAt(i)).equals('*')
                    || Character.valueOf(line.charAt(i)).equals(',')
                    || Character.valueOf(line.charAt(i)).equals('(')
                    || Character.valueOf(line.charAt(i)).equals(')')
                    || (line.length() - 1 == i)) {

                if (line.length() - 1 == i) {
                    if (!(Character.valueOf(line.charAt(i)).equals(',')
                            || Character.valueOf(line.charAt(i)).equals('+')
                            || Character.valueOf(line.charAt(i)).equals('-')
                            || Character.valueOf(line.charAt(i)).equals('*')
                            || Character.valueOf(line.charAt(i)).equals(',')
                            || Character.valueOf(line.charAt(i)).equals('(')
                            || Character.valueOf(line.charAt(i)).equals(')'))) {
                        tokenPart += line.charAt(i);
                    }
                }

                if (!tokenPart.isEmpty()) {

                    if (tokenPart.equals("sumby")) {
                        tokenList.add(new SumByToken());
                    } else if (tokenPart.equals("sum")) {
                        tokenList.add(new SumToken());

//                    } else if (Character.valueOf(line.charAt(i)).equals('+')) {
//                        tokenList.add(new PlusToken());
//                    } else if (Character.valueOf(line.charAt(i)).equals('-')) {
//                        tokenList.add(new MinusToken());
//                    } else if (Character.valueOf(line.charAt(i)).equals('*')) {
//                        tokenList.add(new MultiplyToken());
//                    } else if (Character.valueOf(line.charAt(i)).equals('/')) {
//                        tokenList.add(new DivideToken());
                    } else {

                        try {
                            double v = Double.parseDouble(tokenPart);
                            tokenList.add(new DigitToken(BigDecimal.valueOf(v)));
                        } catch (Exception ex) {
                            tokenList.add(new VariableToken(tokenPart, varsMap));
                        }
                    }
                    tokenPart = "";
                }

                if (Character.valueOf(line.charAt(i)).equals(',')) {
                    tokenList.add(new ColonToken());
                } else if (Character.valueOf(line.charAt(i)).equals('(')) {
                    tokenList.add(new LeftParenthesisToken());
                } else if (Character.valueOf(line.charAt(i)).equals(')')) {
                    tokenList.add(new RightParenthesisToken());
                } else if (Character.valueOf(line.charAt(i)).equals('+')) {
                    tokenList.add(new PlusToken());
                } else if (Character.valueOf(line.charAt(i)).equals('-')) {
                    tokenList.add(new MinusToken());
                } else if (Character.valueOf(line.charAt(i)).equals('*')) {
                    tokenList.add(new MultiplyToken());
                }

            } else  {
                tokenPart += line.charAt(i);
            }
        }
    }

}

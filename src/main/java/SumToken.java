import java.math.BigDecimal;

public class SumToken extends Token {


    public SumToken() {
        super(TokenType.SUM, "SUM");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 2);
        BigDecimal sumValue = BigDecimal.ZERO;
        while (compileContext.getTokensList().get(compileContext.getCurrentIndex()).getTokenType() != TokenType.RIGHT_PARENTHESIS) {
            Token token = compileContext.getTokensList().get(compileContext.getCurrentIndex());
            if (token.getTokenType() == TokenType.DIGIT || token.getTokenType() == TokenType.VARIABLE) {
                sumValue = sumValue.add((BigDecimal)token.getValue());
            }
            compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        }
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(new DigitToken(sumValue));
    }
}

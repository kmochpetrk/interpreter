public class DivideToken extends Token{

    public DivideToken() {
        super(TokenType.DIV, "/");
    }

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        compileContext.getStack().push(this);
    }
}

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class CompileContext {
    private int currentIndex = 0;
    private List<Token> tokensList;
    private Stack<Token> stack = new Stack<Token>();
    private Map<String, BigDecimal> varsMap;
    private List<Timeseries> timeseries = new ArrayList<>();

    public CompileContext(List<Token> tokensList, Map<String, BigDecimal> varsMap) {
        this.tokensList = tokensList;
        this.varsMap = varsMap;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public List<Token> getTokensList() {
        return tokensList;
    }

    public void setTokensList(List<Token> tokensList) {
        this.tokensList = tokensList;
    }

    public Stack<Token> getStack() {
        return stack;
    }

    public Map<String, BigDecimal> getVarsMap() {
        return varsMap;
    }

    public List<Timeseries> getTimeseries() {
        return timeseries;
    }

    public void setTimeseries(List<Timeseries> timeseries) {
        this.timeseries = timeseries;
    }
}

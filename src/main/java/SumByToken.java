import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SumByToken extends Token{

    public SumByToken() {
        super(TokenType.SUMBY, "SUMBY");
    }

    private Map<BigDecimal, BigDecimal> result;

    @Override
    public void interpret(CompileContext compileContext) {
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 2);

        VariableToken tokenByVar = (VariableToken)compileContext.getTokensList().get(compileContext.getCurrentIndex());

        String name = tokenByVar.getName();

        Map<BigDecimal, BigDecimal> cacheMap = new HashMap<>();


        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        while (compileContext.getTokensList().get(compileContext.getCurrentIndex()).getTokenType() != TokenType.RIGHT_PARENTHESIS) {
            Token token = compileContext.getTokensList().get(compileContext.getCurrentIndex());
            if (token.getTokenType() == TokenType.DIGIT || token.getTokenType() == TokenType.VARIABLE) {
                List<Timeseries> timeseries = compileContext.getTimeseries();
                for (Timeseries timeseries1 : timeseries) {

                    BigDecimal key = BigDecimal.valueOf((Integer)timeseries1.getFields().get(name));
                    final BigDecimal bigDecimal = cacheMap.get(key);
                    if (bigDecimal == null) {
                        cacheMap.put(key, BigDecimal.valueOf((Integer)timeseries1.getFields().get(((VariableToken)token).getName())));
                    } else {
                        cacheMap.put(key, (BigDecimal.valueOf((Integer)timeseries1.getFields().get(((VariableToken)token).getName()))).add(bigDecimal));
                    }
                }
            }
            compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        }
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
        this.result = cacheMap;
        compileContext.getStack().push(this);
    }

    public Map<BigDecimal, BigDecimal> getResult() {
        return result;
    }
}

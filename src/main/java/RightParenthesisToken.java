public class RightParenthesisToken extends Token{

    public RightParenthesisToken() {
        super(TokenType.RIGHT_PARENTHESIS, ")");
    }

    @Override
    public void interpret(CompileContext compileContext) {

        processStack(compileContext);
        Token pop = compileContext.getStack().pop();
        compileContext.setCurrentIndex(compileContext.getCurrentIndex() + 1);
    }
}
